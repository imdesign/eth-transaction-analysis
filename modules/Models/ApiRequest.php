<?php

namespace Shared\Models;

use Illuminate\Database\Eloquent\Model;

class ApiRequest extends Model
{
    protected $fillable = [
        'identifier',
        'user_id',
        'amount',
        'url',
        'response',
        'status',
        'message',
    ];

    protected $table = 'api_requests';

    public static function generateHash(): string
    {
        return substr(md5(now()), 0, 12);
    }

    public static function findOne(string $identifier): ?ApiRequest
    {
        return self::where('identifier', $identifier)->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
