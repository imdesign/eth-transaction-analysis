<?php

namespace Shared\Dictionaries;

class CryptoCurrency
{
    public const ETH = 'ETH';

    public const BTC = 'BTC';

    public const FRONTEND_CRYPTOCURRENCIES = [
        self::ETH,
        self::BTC
    ];
}
