<?php

namespace Shared\Dictionaries;

class Currency
{
    public const EUR = 'EUR';

    public const USD = 'USD';

    public const GBP = 'GBP';

    public const FRONTEND_CURRENCIES = [
        self::EUR,
        self::USD,
        self::GBP,
    ];
}
