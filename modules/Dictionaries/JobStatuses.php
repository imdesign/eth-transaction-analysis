<?php

namespace Shared\Dictionaries;

class JobStatuses
{
    public const TYPE_SENT = 'sent';

    public const TYPE_RECEIVED = 'received';

    public const TYPE_IN_PROGRESS = 'in_progress';

    public const TYPE_COMPLETED = 'completed';

    public const TYPE_FAILED = 'failed';
}
