<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\Etherscan\EtherscanApiClient;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;

class EtherscanApiClientTest extends TestCase
{
    public function testGetLatestBlock()
    {
        $mock = new MockHandler([
            new Response(200, [], json_encode(['result' => '0x10d4f'])),
            new ClientException(
                'Error Communicating with Server',
                new Request('GET', 'test'),
                new Response(400, [], 'Bad Request')
            ),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $etherscanClient = new EtherscanApiClient();
        $etherscanClient->client = $client;

        $response = $etherscanClient->getLatestBlock();
        $this->assertEquals('0x10d4f', $response['result']);

        $response = $etherscanClient->getLatestBlock();
        $this->assertStringContainsString('Error Communicating with Server', $response);
    }
}
