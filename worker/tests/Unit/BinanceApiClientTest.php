<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\Binance\BinanceApiClient;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

class BinanceApiClientTest extends TestCase
{
    public function testGetCurrentPairPrice()
    {
        $mock = new MockHandler([
            new Response(200, [], '{"symbol":"ETHEUR","price":"30000.00"}'),
            new RequestException("Error Communicating with Server", new Request('GET', 'test'))
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new Client(['handler' => $handlerStack]);

        $binanceApiClient = new BinanceApiClient();
        $binanceApiClient->client = $client;

        $response = $binanceApiClient->getCurrentPairPrice('ETHEUR');
        $this->assertNotNull($response);
        $this->assertEquals('30000.00', $response['price']);

        $response = $binanceApiClient->getCurrentPairPrice('INVALID');
        $this->assertNull($response);
    }
}
