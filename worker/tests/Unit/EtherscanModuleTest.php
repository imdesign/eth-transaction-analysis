<?php

namespace Tests\Unit;

use App\Services\Etherscan\EtherscanApiClient;
use App\Services\Etherscan\Module;
use PHPUnit\Framework\TestCase;

class EtherscanModuleTest extends TestCase
{
    public function testConvertWeiToEur()
    {
        $module = new Module(new EtherscanApiClient());

        $weiValue = '0x1BC16D674EC80000';
        $ethToEurRate = 1000.0;

        $result = $module->convertWeiToEur($weiValue, $ethToEurRate);

        $this->assertEquals(2000.0, $result, '', 0.001);
    }

    public function testGetMinMaxPrice()
    {
        $module = new Module(new EtherscanApiClient());

        $price = 100.0;

        [$minPrice, $maxPrice] = $module->getMinMaxPrice($price);

        $this->assertEquals(97.0, $minPrice);
        $this->assertEquals(103.0, $maxPrice);
    }
}
