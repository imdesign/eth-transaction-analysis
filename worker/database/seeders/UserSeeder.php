<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Shared\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
             'name' => 'admin',
             'email' => 'test@gmail.com',
             'password' => Hash::make('Testing123@'),
        ]);
        User::create([
             'name' => 'manager',
             'email' => 'test2@gmail.com',
             'password' => Hash::make('Testing123@'),
        ]);
    }
}

