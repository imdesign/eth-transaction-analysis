<?php

namespace App\Console\Commands;

use Shared\Events\fetchBinanceRate;
use App\Services\Binance\BinanceApiClient;
use Illuminate\Console\Command;
use Shared\Dictionaries\CryptoCurrency;
use Shared\Dictionaries\Currency;

class RunEveryTenSeconds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:scheduler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct(private BinanceApiClient $binanceApiClient)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $start = time();
        while (true) {
            $this->info('Running job at: ' . now());
            $latestPrice = $this->binanceApiClient->getCurrentPairPrice(CryptoCurrency::ETH.Currency::EUR);
            event(new fetchBinanceRate($latestPrice['price']));

            sleep(10);
        }
    }
}
