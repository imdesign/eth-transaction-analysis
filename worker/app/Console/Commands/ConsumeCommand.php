<?php

namespace App\Console\Commands;

use App\Services\Binance\BinanceApiClient;
use App\Services\Etherscan\Module;
use Shared\Dictionaries\CryptoCurrency;
use Shared\Dictionaries\Currency;
use Shared\Events\fetchJobStatuses;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Shared\Dictionaries\JobStatuses;
use Shared\Models\ApiRequest;

class ConsumeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:consume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct(private Module $EtherscanModule, private BinanceApiClient $binanceApiClient)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
        $channel    = $connection->channel();

        $channel->queue_declare('fetch-transactions', false, true, false, false);

        echo " [*] Waiting for messages. To exit press CTRL+C\n";

        $callback = function ($msg) {
            $message = json_decode($msg->body, true);

            $apiRequest = ApiRequest::findOne($message['identifier']);
            $apiRequest->update([
                'status' => JobStatuses::TYPE_RECEIVED,
                'message' => 'Job received by worker',
            ]);
            event(new fetchJobStatuses($apiRequest));

            $price = $apiRequest->amount;
            [$minPrice, $maxPrice] = $this->EtherscanModule->getMinMaxPrice($price);
            $latestTransactions = $this->EtherscanModule->getRecentEthTransactions($apiRequest->identifier);
            $transactionsToDB = [];
            foreach($latestTransactions as $transaction) {
                $weiValue = $transaction['value'];
                $ethToEurRate = $this->binanceApiClient->getCurrentPairPrice(CryptoCurrency::ETH.Currency::EUR)['price'];
                $transactionConvertedPrice = $this->EtherscanModule->convertWeiToEur($weiValue, $ethToEurRate);

                if($transactionConvertedPrice >= $minPrice && $transactionConvertedPrice <= $maxPrice) {
                    $transaction['converted_price'] = $transactionConvertedPrice;
                    $transactionsToDB[] = $transaction;
                }
            }

            $apiRequest->update([
                'response' => json_encode($transactionsToDB),
                'status' => JobStatuses::TYPE_COMPLETED,
                'message' => 'Request completed',
            ]);
            event(new fetchJobStatuses($apiRequest));
        };

        $channel->basic_consume('fetch-transactions', '', false, true, false, false, $callback);

        while ($channel->is_open()) {
            $channel->wait();
        }
    }
}
