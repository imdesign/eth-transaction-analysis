<?php

namespace App\Http\Controllers;

use App\Services\Binance\BinanceApiClient;
use App\Services\Etherscan\Module;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Shared\Models\ApiRequest;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function __construct(private Module $EtherscanModule, private BinanceApiClient $binanceApiClient)
    {
    }

    public function index()
    {
       $price = 100;

       [$minPrice, $maxPrice] = $this->EtherscanModule->getMinMaxPrice($price);
       $latestTransactions = $this->EtherscanModule->getRecentEthTransactions('16a06fb23fd6');
       $transactionsToDB = [];
       foreach($latestTransactions as $transaction) {
           $weiValue = $transaction['value'];
           $ethToEurRate = $this->binanceApiClient->getCurrentPairPrice('ETHUSDT')['price'];
           $transactionConvertedPrice = $this->EtherscanModule->convertWeiToEur($weiValue, $ethToEurRate);

          if($transactionConvertedPrice >= $minPrice && $transactionConvertedPrice <= $maxPrice) {
              $transaction['converted_price'] = $transactionConvertedPrice;
              $transactionsToDB[] = $transaction;
          }
       }
       $apiRequest = ApiRequest::findOne('16a06fb23fd6');
       $apiRequest->update([
           'response' => json_encode($transactionsToDB),
           'status' => 'completed',
           'message' => 'Request completed',
       ]);
    }
}
