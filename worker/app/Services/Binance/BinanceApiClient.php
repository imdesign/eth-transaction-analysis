<?php

namespace App\Services\Binance;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class BinanceApiClient
{
    public $client;
    protected $apiKey;
    protected $apiSecret;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://api.binance.com']);
        $this->apiKey = env('BINANCE_API_KEY');
        $this->apiSecret = env('BINANCE_API_SECRET');
    }

    /**
     * Get current market prices.
     *
     * @return array|null
     */
    public function getCurrentPairPrice(string $pair)
    {
        try {
            $response = $this->client->request('GET', '/api/v3/ticker/price?symbol=' . $pair);
            return json_decode($response->getBody()->getContents(), true);
        } catch (GuzzleException $e) {
            return null;
        }
    }
}
