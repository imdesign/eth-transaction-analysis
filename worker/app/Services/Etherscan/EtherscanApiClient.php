<?php

namespace App\Services\Etherscan;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class EtherscanApiClient
{
    public $client;
    protected $apiKey;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://api.etherscan.io']);
        $this->apiKey = env('ETHERSCAN_API_KEY');
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function getLatestBlock()
    {
        try {
            $response = $this->client->request('GET', '/api', [
                'query' => [
                    'module' => 'proxy',
                    'action' => 'eth_blockNumber',
                    'apikey' => $this->getApiKey()
                ]
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (GuzzleException $e) {
            return $e->getMessage();
        }
    }

    public function getBlockByNumber(string $blockNumber)
    {
        try {
            $response = $this->client->request('GET', '/api', [
                'query' => [
                    'module' => 'proxy',
                    'action' => 'eth_getBlockByNumber',
                    'tag' => '0x' . dechex($blockNumber),
                    'boolean' => 'true',
                    'apikey' => $this->getApiKey()
                ]
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (GuzzleException $e) {
            return $e->getMessage();
        }
    }
}
