<?php

namespace App\Services\Etherscan;

use GuzzleHttp\Exception\GuzzleException;
use Shared\Dictionaries\JobStatuses;
use Shared\Events\fetchJobStatuses;
use Shared\Models\ApiRequest;

class Module {

    const API_RATE_LIMIT_PER_SECOND = 5;
    const TIME_LIMITER = 1;

    public function __construct(private EtherscanApiClient $client)
    {}

    public function getRecentEthTransactions(string $requestIdentifier, int $timeInMinutes = self::TIME_LIMITER)
    {
        $timeAgo = now()->subMinutes($timeInMinutes)->timestamp;

        $apiRequest = ApiRequest::findOne($requestIdentifier);
        $apiRequest->update([
            'status' => JobStatuses::TYPE_IN_PROGRESS,
            'message' => 'Request in progress...',
        ]);

        event(new fetchJobStatuses($apiRequest));

        try {
            $requestIterator = 1;
            $latestBlockResponse = $this->client->getLatestBlock();
            $latestBlockNumber = hexdec((string) $latestBlockResponse['result']);

            // Fetch blocks and transactions
            for ($blockNumber = $latestBlockNumber; $blockNumber > 0; $blockNumber--) {
                if($requestIterator % self::API_RATE_LIMIT_PER_SECOND === 0) {
                    sleep(1);
                }
                $requestIterator++;
                $blockResponse = $this->client->getBlockByNumber($blockNumber);

                if (isset($blockResponse['result'])) {
                    $blockTimestamp = hexdec($blockResponse['result']['timestamp']);

                    // Check if the block is older than 10 minutes
                    if ($blockTimestamp < $timeAgo) {
                        break;
                    }

                    // Add transactions from this block
                    foreach ($blockResponse['result']['transactions'] as $transaction) {
                        $transactions[] = $transaction;
                    }
                }
            }

            return $transactions;
        } catch (GuzzleException $e) {
            return null;
        }
    }

    public function convertWeiToEur(string $weiValue, float $ethToEurRate): float {
        // Convert hexadecimal Wei value to decimal
        $weiValueDecimal = hexdec($weiValue);

        // Convert Wei to ETH (1 ETH = 1e18 Wei)
        $ethValue = $weiValueDecimal / 1e18;

        // Convert ETH to EUR
        $eurValue = $ethValue * $ethToEurRate;

        return $eurValue;
    }

    public function getMinMaxPrice(float $price): array {
        $minPrice = $price - ($price * 0.03);
        $maxPrice = $price + ($price * 0.03);
        return [$minPrice, $maxPrice];
    }
}
