<?php

use \Illuminate\Support\Facades\Route;
use App\Http\Controllers\Transaction\FormController;

Route::post('/transaction', [FormController::class, 'receive'])->name('transaction.receive');
Route::post('/trigger-alert', [FormController::class, 'alert'])->name('transaction.alert');
