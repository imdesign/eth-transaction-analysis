<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\BufferedOutput;
use \Shared\Models\ApiRequest;
use Prologue\Alerts\Facades\Alert;

class FormController
{
    public function receive(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric',
        ]);

        $commandRequestObject = [
            'amount' => $request->amount,
            'user_id' => $request->user,
        ];

        $output = new BufferedOutput();
        Artisan::call('rabbitmq:publish', $commandRequestObject);

        $commandResponse = $output->fetch();

        return $commandResponse;
    }

    public function alert(Request $request)
    {
        $request->validate([
           'identifier' => 'required|string',
        ]);

        $apiRequest = ApiRequest::findOne($request->identifier);

        Alert::success($apiRequest->identifier. ' | '. $apiRequest->message)->flash();

        return response()->json(['message' => $apiRequest->identifier. ' | '. $apiRequest->message]);
    }
}
