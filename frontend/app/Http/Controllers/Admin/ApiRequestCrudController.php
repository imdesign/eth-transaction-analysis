<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ApiRequestRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\ApiRequest;

/**
 * Class ApiRequestCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ApiRequestCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(ApiRequest::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/api-request');
        CRUD::setEntityNameStrings('api request', 'api requests');
    }

    protected function setupShowOperation()
    {
        CRUD::column('identifier')->type('string');
        CRUD::column('url')->type('decimal');
        CRUD::column('amount')->type('decimal');
        CRUD::column('status')->type('string');
        CRUD::column('message')->type('string');
        CRUD::column('response')->view_namespace('json-field-for-backpack::fields')->modes(['form', 'tree', 'code'])->type('json');
        CRUD::column('created_at')->type('datetime');
        CRUD::column('updated_at')->type('datetime');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('identifier')->type('string');
        CRUD::column('amount')->type('decimal');
        CRUD::column('status')->type('string');
        CRUD::column('message')->type('string');
        CRUD::column('created_at')->type('datetime');
        /**
         * Columns can be defined using the fluent syntax:
         * - CRUD::column('price')->type('number');
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ApiRequestRequest::class);
        CRUD::setFromDb(); // set fields from db columns.

        /**
         * Fields can be defined using the fluent syntax:
         * - CRUD::field('price')->type('number');
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        $this->crud->addField([
              'name'  => 'response',
              'type'  => 'json',
              'view_namespace' => 'json-field-for-backpack::fields',
              'modes' => ['form', 'tree', 'code'],
        ]);
    }
}
