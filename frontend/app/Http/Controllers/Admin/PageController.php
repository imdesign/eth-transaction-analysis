<?php

namespace App\Http\Controllers\Admin;

use Shared\Models\ApiRequest;

class PageController
{
    public function index()
    {
        $requests = ApiRequest::orderBy('created_at', 'desc')->get();

        return view('admin.dashboard', ['requests' => $requests]);
    }
}
