<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Shared\Dictionaries\JobStatuses;
use Shared\Events\fetchJobStatuses;
use Shared\Models\ApiRequest;

class PublishCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:publish {amount} {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $paramAmount = $this->argument('amount');
        $paramUserId = $this->argument('user_id');

        $connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
        $channel    = $connection->channel();

        $channel->queue_declare('fetch-transactions', false, true, false, false);

        $requestParam = [
            'amount' => $paramAmount,
            'identifier' => ApiRequest::generateHash(),
            'user_id' => $paramUserId,
            'status' => JobStatuses::TYPE_SENT,
            'message' => 'Job sent to queue',
        ];
        ApiRequest::create($requestParam);

        $msg = new AMQPMessage(json_encode($requestParam));
        $channel->basic_publish($msg, '', 'fetch-transactions');


        $channel->close();
        $connection->close();
    }
}
