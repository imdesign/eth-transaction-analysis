@extends(backpack_view('blank'))

@section('content')
    <div id="alertContainer"></div>

    <h4>Current Binance ETH/EUR exchange rate: <span id="exchangeRate" style="color: #00a65a;"></span></h4>

    <form id="dashboardForm" method="POST" action="{{ route('transaction.receive') }}" class="mb-40">
        @csrf
        <div class="inline-flex w-100-vh align-flex-end gap-20">
            <div class="w-100-vh">
                <label for="amount">Amount</label>
                <input type="number" step="0.01" class="form-control" id="amount" name="amount" placeholder="Enter amount in EUR" required>
                <input type="hidden" name="user" required value="{{ auth('backpack')->user()->id }}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>

    <h4>History</h4>

    <div id="accordion">
        @foreach($requests as $request)
            <div class="card" data-identifier="{{$request->identifier}}">
                <div class="card-header" id="{{$request->identifier}}">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$request->identifier}}" aria-expanded="true" aria-controls="collapse{{$request->identifier}}">
                            {{$request->created_at}} | Request {{$request->identifier}} by {{$request->user->name}} | Status: <span id="status">{{$request->status}}</span> | Message: {{$request->message}}
                        </button>
                    </h5>
                </div>

                <div id="collapse{{$request->identifier}}" class="collapse" aria-labelledby="{{$request->identifier}}" data-parent="#accordion">
                    <div class="card-body">
                        {{$request->response}}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    @vite('resources/js/app.js')
@endsection
