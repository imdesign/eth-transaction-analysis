const exchangeRate = document.getElementById('exchangeRate');

document.addEventListener('DOMContentLoaded', function () {
    window.Echo.channel('binance-rate')
    .listen('.Shared\\Events\\fetchBinanceRate', (e) => {
        let numberValue = parseFloat(e.data);
        exchangeRate.innerHTML = Number(numberValue.toFixed(2));
        console.log('Event received:', e);
    })

    window.Echo.channel('rabbitmq')
    .listen('.Shared\\Events\\fetchJobStatuses', (e) => {
        console.log('Event received:', e);
        displayAlert(e.apiRequest.identifier +' | '+e.apiRequest.message);
        triggerAlert(e.apiRequest.identifier);
    });
});

document.getElementById('dashboardForm').addEventListener('submit', function(e) {
    e.preventDefault();
    submitForm();
});

function submitForm() {
    let xhr = new XMLHttpRequest();
    let formData = new FormData(document.getElementById('dashboardForm'));

    xhr.open('POST', '/transaction', true);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    xhr.setRequestHeader('X-CSRF-TOKEN', document.querySelector('meta[name="csrf-token"]').getAttribute('content'));

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            var response = JSON.parse(xhr.responseText);
            console.log(response);
        } else {
            console.error('The request failed!');
        }
    };

    xhr.send(formData);
}

function triggerAlert(identifier) {
    const data = new FormData();
    data.append('identifier', identifier);

    fetch('/trigger-alert', {
        method: 'POST',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content') // CSRF token for Laravel
        },
        body: data
    }).then(response => {
        if (response.ok) {
            return response.json();
        }
        throw new Error('Network response was not ok.');
    }).then(data => {
        displayAlert(data.message);
        console.log(data.message);
    }).catch(error => console.error('Error:', error));
}

function displayAlert(message) {
    const alertContainer = document.getElementById('alertContainer');
    const alertElement = document.createElement('div');
    alertElement.className = 'alert alert-success'; // Set appropriate classes
    alertElement.textContent = message;
    alertContainer.appendChild(alertElement);
}
