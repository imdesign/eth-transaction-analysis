### Architecture:
![Alt text](readme/architecture_.png "Architecture")
<img src="readme/containers.png" alt="Alt text for the image" width="300"/>

### Application UI:
![Alt text](readme/interface.png "Form interface")
![Alt text](readme/view_transaction_data.png "Transaction details")

### Installation prerequisites:
- [Docker desktop](https://www.docker.com/products/docker-desktop/)
- [Composer](https://getcomposer.org/) (to fetch Laravel Sail image)

### Run project locally:
```
cd frontend 
composer require laravel/sail --dev
cd ../docker
docker-compose -p transaction-analysis up
```
Additional commands to run from worker container after initial setup:
```
docker exec -it transaction-analysis_worker bash
php artisan migrate
php artisan websockets:serve
Php artisan app:scheduler
php artisan rabbitmq:consume
php artisan db:seed --class=UserSeeder
```
after running seeder you can login on frontend with two pre-created accounts:
* email: test@gmail.com
* password: Testing123@
---
* email: test2@gmail.com
* password: Testing123@
---

### Access project locally:

- Frontend: http://localhost:81/
- Worker: http://localhost:82/
- Websockets logger: http://localhost:82/laravel-websockets
- RabbitMQ client: http://localhost:15672/

### Run tests:
```
docker exec -it transaction-analysis_worker bash
php artisan test
```
